#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/7 1:45 下午
# @Author  : jing.yang
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class TestWait:
    def setup(self):
        self.driver = webdriver.Chrome()
        self.driver.get("http://www.baidu.com")
        self.driver.implicitly_wait(3)

    def test_wait(self):
        self.driver.find_element(By.ID,"kw").click()
        expected_conditions.element_to_be_clickable(By.ID,"kw")
        def wait(x):
            return len(self.driver.find_element(By.ID,"kw")) >=1
        WebDriverWait(self.driver,10).until(expected_conditions.element_to_be_clickable(By.ID,"kw"))

