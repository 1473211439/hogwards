#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/15 16:54
# @Author  : jing.yang

# hamcrest断言
import requests
from hamcrest import *


def test_hamcrest():
    url = "https://ceshiren.com/categories"
    r = requests.get(url)
    print(r.text)
    assert r.status_code == 200
    assert_that(r.json()['aaa']['ddd'][0], equal_to('hogwwarts'))