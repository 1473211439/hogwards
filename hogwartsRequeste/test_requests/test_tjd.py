#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/15 15:14
# @Author  : jing.yang

import pystache
'''
https://github.com/defunkt/pystache
pystache是小胡子（mustache）的python版本插件。它的特点是轻量级、使用简单。
pystache支持变量替换、列表遍历、条件判断等。更多的使用方法见官方文档。
'''

import pystache

