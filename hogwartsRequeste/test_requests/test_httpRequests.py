#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/9 16:45
# @Author  : jing.yang
import requests
from jsonpath import jsonpath



class TestRequests:
    def test_get(self):
        r = requests.get('https://httpbin.testing-studio.com/get')
        print(r.status_code)
        print(r.text)
        print(r.json())
        assert r.status_code == 200

    def test_post(self):
        r = requests.post('https://httpbin.testing-studio.com/post')
        print(r.url)
        print(r.headers)
        print(r.content)
        assert  r.url == 'https://httpbin.testing-studio.com/post'

    #get方法传递参数
    def test_query(self):
        paycode = {
            "level":1,
            "name":"slient"
        }
        r = requests.get('https://httpbin.testing-studio.com/get', params=paycode)
        print(r.text)
        assert r.status_code ==200

    #form请求参数构造
    def test_post_form(self):
        payload = {
            "level":2,
            "name":"jingjing"
        }
        r = requests.post('https://httpbin.testing-studio.com/post',data=payload)
        print(r.text)
        assert r.status_code ==200

    #json请求数据结构
    def test_post_json(self):
        payload = {
            "level":3,
            "name":"jingjing666",
            "age":23
        }
        r = requests.post('https://httpbin.testing-studio.com/post',json=payload)
        print(r.text)
        assert r.status_code ==200
        assert r.json()["json"]["level"]==3


    #文件上传
    def test_file(self):
        file_test = {'file_test':open('test1.xls','rb')}
        r = requests.post('https://httpbin.testing-studio.com/post',files = file_test)
        print(r.text)
        assert r.status_code == 200


    #header
    def test_header(self):
        r = requests.get('https://httpbin.testing-studio.com/get',headers={"h":"header dome"} )
        print(r.status_code)
        print(r.text)
        print(r.json())
        assert r.status_code == 200
        assert r.json()["headers"]["H"] =="header dome"

    def test_hogwarts_json(self):
        url = "https://ceshiren.com/categories"
        r = requests.get(url)
        print(r.text)
        assert r.status_code==200
        assert jsonpath(r.json(),'$..name')[0]=="hugwarts"







