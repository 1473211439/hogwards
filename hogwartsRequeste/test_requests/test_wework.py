#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/16 11:53
# @Author  : jing.yang

'''
获取企业微信的token
'''
import requests

def test_get_token():
    corpid = 'ww54ff96d908a23f9b'
    corpsecret= 'zmCCaCTD2kL36wgVNqDDnE6y1GV1hw8yvSApExhiX8M'
    url = f"https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={corpid}&corpsecret={corpsecret}"
    r = requests.get(url=url)
    # print(r.json().get("access_token"))
    token = r.json().get("access_token")
    return token

#查看标签
def test_get_tag():
    token=test_get_token()
    url_1=f"https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token={token}&tagid={13}"
    r = requests.get(url=url_1)
    print(r.json())

#创建标签
def test_creat_tag():
    token = test_get_token()
    url = f"https://qyapi.weixin.qq.com/cgi-bin/tag/create?access_token={token}"
    data = {
        "tagname": "UI_slient1",

    }
    r =requests.post(url=url,json=data)
    print(r.json())

#删除标签
def test_delete_tag():
    token = test_get_token()
    url = f"https://qyapi.weixin.qq.com/cgi-bin/tag/delete?access_token={token}&tagid={13}"
    r = requests.get(url=url)
    print(r.json())

#更新标签
def test_updata_tag():
    token = test_get_token()
    url = f"https://qyapi.weixin.qq.com/cgi-bin/tag/update?access_token={token}"
    data = {
        "tagid": 13,
        "tagname": "UI design_1234564"
    }
    r = requests.post(url=url, json=data)
    print(r.json())