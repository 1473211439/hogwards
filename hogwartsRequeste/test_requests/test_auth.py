#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/15 16:43
# @Author  : jing.yang
'''
认证体系
'''
import requests
from requests.auth import HTTPBasicAuth

def test_auth_01():
    url = "https://httpbin.testing-studio.com/basic-auth/slient/123456"
    r = requests.get(url=url,auth=HTTPBasicAuth("slient","123456"))
    print(r.text)
