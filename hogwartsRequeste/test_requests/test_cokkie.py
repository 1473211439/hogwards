#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/15 16:51
# @Author  : jing.yang

# Cookie
import requests


def test_cookie1():
    url = "https://httpbin.testing-studio.com/cookies"
    header = {
        "Cookie": "slient",  # "Cookie"要大写
        'User-Agent': 'tjd12334'
    }
    r = requests.get(url=url, headers=header)
    print(r.request.headers)
    assert r.status_code == 200


def test_cookie2():
    url = "https://httpbin.testing-studio.com/cookies"
    header = {
        'User-Agent': 'tjd12334'
    }
    cookie_data = {
        "hogwarts": "school",
        "teacher": "AD"
    }
    r = requests.get(url=url, headers=header, cookies=cookie_data)
    print(r.request.headers)
    assert r.status_code == 200
