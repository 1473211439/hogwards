#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/20 15:08
# @Author  : jing.yang
import requests
from faker import Faker


class BaseApi:
    #把常量放到类变量中
    CORPID = 'ww54ff96d908a23f9b'
    CORPSECRET = 'zmCCaCTD2kL36wgVNqDDnE6y1GV1hw8yvSApExhiX8M'
    BASE_URL = 'https://qyapi.weixin.qq.com/cgi-bin/'
    def __init__(self):
        self.token = self.get_token()
        self.fake = Faker("zh_cn")
        self.mobile = self.fake.phone_number()
        self.name = self.fake.name()
        # self.email = self.faker.ascii_free_email()

    #获取token
    def get_token(self):
        url = self.BASE_URL+f"/gettoken?corpid={self.CORPID}&corpsecret={self.CORPSECRET}"
        r = requests.get(url=url)
        return r.json().get("access_token")

    def send(self,method, url, **kwargs):
        '''
        封装发送请求
        :param method: 请求方式
        :param url: 路由地址
        :param kwargs: 其他参数
        :return:
        '''
        url = self.BASE_URL+ url
        return requests.request(method,url,**kwargs)