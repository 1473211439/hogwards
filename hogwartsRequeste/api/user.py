#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/20 16:19
# @Author  : jing.yang
import requests

# from hogwartsRequeste import BaseApi
from hogwartsRequeste.api.base_api import BaseApi


class User(BaseApi):

    # 查询（读取）成员
    def get_user(self,userid):
        # url = f"https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token={self.token}&userid={userid}"
        # r =requests.get(url=url)
        r = self.send("get",f"user/get?access_token={self.token}&userid={userid}")
        return r

    # 创建成员
    def add_user(self,userid,email,**kwargs):

        data = {
            "userid":userid,
            "name":self.name,
            "department":[3],
            "mobile": self.mobile,
            "email": email
        }
        data.update(kwargs)
        print(data)
        # url = f"https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token={self.token}"
        # r = requests.post(url=url,json=data)
        new_user  = self.send("post",f"user/create?access_token={self.token}",json=data)
        return new_user


    # 修改成员
    def updata_user(self,userid):
        # url = f"https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token={self.token}"
        data = {
            "userid": userid,
            "name": self.name,
            "department": [3],
        }
        # r = requests.post(url=url,json=data)
        r = self.send("post",f"user/update?access_token={self.token}",json=data)
        return r

    #删除成员
    def delete_user(self,get_userid):
        # url = f"https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token={self.token}&userid={get_userid}"
        # r = requests.get(url=url)
        r = self.send("get",f"user/delete?access_token={self.token}&userid={get_userid}")
        return r


    #创建部门，获取部门ID
    def get_department(self):
        url = f"https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token={self.token}"
        data = {
            "name": "tjd_test02",
            "parentid": 1
        }
        r = requests.post(url=url,json=data)
        print(r.json())




