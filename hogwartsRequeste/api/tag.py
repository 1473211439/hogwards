#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/19 15:54
# @Author  : jing.yang



'''
1，测试接口放到类中，可以让其他方法进行复用
2，便于统一处理，重复逻辑可以提取复用
3.必填参数，方在函数参数中，非必填放到kwargs 中
4.把base url 封装到父类中

'''
from hogwartsRequeste.api.base_api import BaseApi


class Tag(BaseApi):

    #创建tag
    def add(self,tagname,**kwargs):
        '''
        添加标签
        :param tagname:标签名
        :param kwargs: tagid ，默认会自动生成
        :return:
        kwargs 接受任意参数
        *args 任意位置传参
        '''
        # url = self.BASE_URL+f"tag/create?access_token={self.token}"
        data = {
            "tagname": tagname,

        }
        #把其他非必填参数更新到数据中
        data.update(kwargs)
        # r = requests.post(url=url, json=data)
        r = self.send("post",f"tag/create?access_token={self.token}",json=data)
        return r



    #删除tag
    def delete(self,tagid):
        '''
        :param tagid: 标签ID
        :return:
        '''
        # url = self.BASE_URL+f"tag/delete?access_token={self.token}&tagid={tagid}"
        # r = requests.get(url=url)
        r = self.send("get",f"tag/delete?access_token={self.token}&tagid={tagid}")
        return r

    #更新tag
    def updata(self,tagid,tagname):
        # url = self.BASE_URL+f"tag/update?access_token={self.token}"
        data = {
            "tagid": tagid,
            "tagname": tagname
        }
        # r = requests.post(url=url, json=data)
        r = self.send("post",f"tag/update?access_token={self.token}",json=data)
        return r

    #查看tag
    def get(self):
        # url_1 =self.BASE_URL+ f"tag/list?access_token={self.token}"
        # r = requests.get(self.BASE_URL+ f"tag/list?access_token={self.token}")
        r = self.send("get",f"tag/list?access_token={self.token}")
        return r



    def is_in_taglist(self,tag_id):
        """
        判断 tag_id 是否在 taglist 中
        :param tag_id:
        :return:
        """
        tag_list = self.get().json().get("taglist")
        for tag in tag_list:

            if tag_id == tag.get("tagid"):
                return True
        return False
