#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/20 16:31
# @Author  : jing.yang
import allure

# from hogwartsRequeste import User
from hogwartsRequeste.api.user import User


@allure.feature("企业微信成员管理测试")
class TestUser:
    def setup(self):
        self.user = User()


    @allure.title("读取成员")
    def test_get_user(self,get_userid,get_email):
        """
        读取成员
        :param get_userid: userid
        :param get_name:  name
        :param get_email:  email
        :return:
        """
        self.user.add_user(get_userid,get_email)
        self.user.get_user(get_userid).json()
        assert self.user.get_user(get_userid).json().get("errcode")==0


    @allure.title("添加成员")
    def test_add_user(self,get_userid,get_email):
        '''
        添加成员
        :param get_userid: 成员ID  从conftest.py文件中 get_userid获取
        :param get_name: 成员姓名，从conftest.py文件中 get_name获取
        :param get_email:
        :return:
        '''
        r = self.user.add_user(get_userid,get_email).json()
        print(r)
        assert r.get("errcode") ==0

    @allure.title("删除成员")
    def test_delete_uesr(self,get_userid, get_email):
        '''
        删除成员
        :param get_userid: userid  用户ID
        :param get_email: email 用户邮箱
        :return:
        '''
        self.user.add_user(get_userid, get_email)
        r = self.user.delete_user(get_userid).json()
        print(r)
        assert r.get("errcode") ==0


    @allure.title("更新成员")
    def test_update_uesr(self,get_userid, get_email):
        '''
        #更新成员
        :param get_userid: userid  用户ID
        :param get_email: email 用户邮箱
        :return:
        '''
        self.user.add_user(get_userid, get_email)
        r = self.user.updata_user(get_userid).json()
        print(r)
        assert r.get("errcode") == 0



    #创建部门
    # def test_get_department(self):
    #     r = self.user.get_department()
    #     print(r)
    #

