#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/19 16:11
# @Author  : jing.yang

# from hogwartsRequeste import Tag
from hogwartsRequeste.api.tag import Tag


class TestTag:
    def setup_class(self):
        self.tag =Tag()

    def test_get(self):
        #返回的数据以json形式展示，需要使用json.dumps(返回内容)，indent=2 缩进
        # print(json.dumps(self.tag.get().json(),indent=2))
        r = self.tag.get()
        assert r.json().get('errcode')==0


    def test_add(self,get_unioc_name):
        # print("唯一名称"+ str(get_unioc_name))
        # print(json.dumps(self.tag.add().json(),indent=2))
        new_tag = self.tag.add(get_unioc_name).json()
        assert new_tag.get("errcode")==0
        assert self.tag.is_in_taglist(new_tag.get("tagid"))
        # tag_list = self.tag.get().get("taglist")
        # assert self.tag.add(tag_name).json().get("tagid") in tag_list


    def test_delete(self,get_unioc_name):
        new_tag = self.tag.add(get_unioc_name)
        print(get_unioc_name)
        assert  self.tag.delete(new_tag.json().get("tagid")).json().get("errcode") == 0
        # print(json.dumps(self.tag.delete().json(),indent=2))


    def test_updata(self,get_unioc_name):
        new_tag = self.tag.add(get_unioc_name)
        assert  self.tag.updata(new_tag.json().get("tagid"),get_unioc_name).json().get("errcode")==0