#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/19 18:47
# @Author  : jing.yang
import threading
import time
import random

import pytest


@pytest.fixture()
def get_unioc_name():
    tag_name = str(time.time()) + threading.currentThread().name
    return tag_name

@pytest.fixture()
def get_userid():
    userid = str(time.time())
    return userid


@pytest.fixture()
def get_email():
    email = str(random.randint(100,1000)) + "@qq.com"
    print(email)
    return email
