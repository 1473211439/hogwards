#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/25 8:29 下午
# @Author  : jing.yang

'''
fixture作用域，默认function
'''


import pytest

#所有对测试用例前执行一次
@pytest.fixture(scope='module')
def login():
    print('login')
    return 'token'

def test_case1(login):
    print('case1')

def test_case2(login):
    print('case2')

def test_case3(login):
    print('case3')