#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/26 8:25 上午
# @Author  : jing.yang
from time import sleep
import pytest


@pytest.mark.run(order=2)
def test_foo():
    sleep(1)
    assert True

@pytest.mark.run(order=1)
def test_bar0():
    assert True

@pytest.mark.run(order=-1)
def test_bar1():
    assert True


@pytest.mark.run(order=-2)
def test_bar2():
    assert True