#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/25 8:35 下午
# @Author  : jing.yang

'''
yield的激活teardown
'''
'''
1、如果不加autouse=True 需要在用到fixture的地方，名字都传递
2、如果在测试用例中需要用到fixture都返回数据，那么一定要加上这个fixture的名字
'''

import pytest

#所有对测试用例前执行一次
@pytest.fixture(scope='module',autouse=True)
def login():
    print('登陆')
    #相当于reture,
    # yield前面的操作，想当与setup,
    # yield的后面的操作相当于teardown
    yield "token=134234342325" #使用yield关节字实现登出的操作，
    print('登出')


def test_case1(login):
    print(login)
    print('case1')

def test_case2():
    print('case2')

def test_case3():
    print('case3')
    print('login')


