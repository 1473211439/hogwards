#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/25 10:31 下午
# @Author  : jing.yang

'''
fixture实现参数化
类似于参数化的功能，也可以通过ids为测试用例起别名
request也是一个fixture
request是内置的fixture
'''
import pytest

#自定义的fixture
@pytest.fixture(params=['tom','jerry','lida'],ids=['name1','name2','name3'])
def login(request):#request是内置的fixture
    myparam = request.param
    print(f"用户名：{myparam}")
    yield myparam
    print('登陆操作')

def test_casel(login):
    print(login)
