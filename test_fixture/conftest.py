#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/25 9:39 下午
# @Author  : jing.yang
'''
执行测试用例用例名称中文转换
'''
import yaml


def pytest_collection_modifyitems(items):
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")
        if 'hook' in item.name:
            item.add_marker(pytest.mark.hook)
        if 'aaa' in item.name:
            item.add_marker(pytest.mark.hook)





'''
理解 conftest.py
在当前目录下创建一个conftest.py文件
1、conftest.py文件不需要导入，直接引用，里面定义的fixture方法
2、就近原则，当前模块>当前目录的conftest.py>父级目录的conftest.py>祖辈目录（不会找兄弟节点）
3、一般来说，conftest.py皴法一些公共方法，比如fixture,hook
'''
import pytest
from pythoncode.calculator import Calculator

#所有对测试用例前执行一次
@pytest.fixture(scope='module',autouse=True)
def login():
    print('登陆')
    #相当于reture,
    # yield前面的操作，想当与setup,
    # yield的后面的操作相当于teardown
    yield "token=134234342325" #使用yield关节字实现登出的操作，
    print('登出')

#测试步骤 链接数据库
@pytest.fixture()
def connectDB():
    print('链接数据库')
    return 'databass --datas'



#获取计算器实例的fixture
@pytest.fixture(scope='class')
def get_calc_object():
    print("开始计算")
    calc = Calculator()
    yield calc
    print("计算完成")






#获取测试数据
def get_calc_data():
    with open('./datas1/calc1.yml') as f:
        totals = yaml.safe_load(f)
    return (totals['datas'],totals['ids'])

#测试 获取数据的方法
def test_getdata():
    print(get_calc_data())

#request.param 实现了数据的获取
#ids 为每条数据起个别名，fixture 实现
@pytest.fixture(params=get_calc_data()[0],ids=get_calc_data()[1])
def get_datas(request):
    return request.param
