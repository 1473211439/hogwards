#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/25 8:24 下午
# @Author  : jing.yang

'''
fixture引用其他对fixture
'''



import pytest

# Arrange
@pytest.fixture
def first_entry():
    return "a"


# Arrange
@pytest.fixture
def order(first_entry):
    return [first_entry]


def test_string(order):
    # Act
    order.append("b")

    # Assert
    assert order == ["a", "b"]