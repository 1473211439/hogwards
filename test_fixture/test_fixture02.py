#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/25 7:32 下午
# @Author  : jing.


import pytest
'''
fixture 与参与同时存在的情况
'''

@pytest.fixture
def login():
    print('login')
    return "token"

@pytest.mark.parametrize('a,b',[
    [1,2],
    [3,1]
])
def test_param(a,b,login):
    print()