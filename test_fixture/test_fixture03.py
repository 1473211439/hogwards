#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/25 8:07 下午
# @Author  : jing.yang
import pytest
'''
autouse=True自动引用，默认是false
'''


#@pytest.fixture(autouse=True) 所有的测试用例自动引用fixture
@pytest.fixture(autouse=True)
def login():
    print('login')
    return 'token'

def test_case1():
    print('case1')

def test_case2():
    print('case2')

def test_case3(login):
    print('case3')