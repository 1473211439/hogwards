#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/25 11:08 下午
# @Author  : jing.yang

'''
通过fixture获取测试数据
'''
import allure



#测试类
@allure.feature('计算器')
class TestCalculator:
    @allure.title("相加功能_{get_datas[0]}_{get_datas[1]}")
    @allure.story('相加功能')
    def test_add(self,get_calc_object,get_datas):
        assert get_datas[2] ==get_calc_object.add(get_datas[0],get_datas[1])
