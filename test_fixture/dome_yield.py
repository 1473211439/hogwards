#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/25 9:24 下午
# @Author  : jing.yang

'''
yield的用法
python里可以实现生成器
'''

def provider():
    for i in range(1,10):
        print('start')
        yield i #相当于return ，返回数据，如不加i  也可以不返回数据
                #记录上一次的执行位置，下一次继续执行后面的位置
        print('end')

p = provider()
# provider(next(p)) #1
# provider(next(p))#2
# provider(next(p))#3

for x in p:
    print(x)