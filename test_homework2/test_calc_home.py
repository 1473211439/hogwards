#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/26 9:24 下午
# @Author  : jing.yang

'''
通过fixture获取测试数据
'''
import logging

import allure
import yaml
import pytest

def get_calc_data():
    '''#获取测试数据'''
    with open('./datas.yml') as f:
        totals = yaml.safe_load(f)
    return (totals['datas'],totals['ids'])


'''
#测试 获取数据的方法
'''
def test_getdata():
    print(get_calc_data())

#request.param 实现了数据的获取
#ids 为每条数据起个别名，fixture 实现
@pytest.fixture(params=get_calc_data()[0],ids=get_calc_data()[1])
def get_datas(request):
    return request.param




@allure.feature('计算器测试')
class TestCalculator:
    '''测试计算机加法功能'''
    @allure.title("相加功能_{get_datas[0]}_{get_datas[1]}")
    @allure.story('相加功能')
    def test_add(self,get_calc_object,get_datas):
        logging.info(get_datas)
        assert get_datas[2] ==round(get_calc_object.add(get_datas[0],get_datas[1]),2)


    @pytest.mark.parametrize('a,b,expect', [
        [0.1, 0.1, 0.2],
        [0.1, 0.2, 0.3]
    ])
    @allure.story("浮点数相加")
    def test_add_float(self, a, b, expect):
        logging.info("小数相加保留小数点后两位")
        assert expect == round((a + b), 2)


    @pytest.mark.div
    def test_div(self,get_calc_object):
        assert 1 == get_calc_object.div(1,1)

