#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/26 9:25 下午
# @Author  : jing.yang

import logging
import pytest
from pythoncode.calculator import Calculator

@pytest.fixture()
def get_calc_object():
    logging.info("【开始计算】")
    yield Calculator()
    logging.info("【计算结束】")



'''
处理中文的unicode显示问题
'''
def pytest_collection_modifyitems(items):
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")