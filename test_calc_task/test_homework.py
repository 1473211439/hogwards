#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/24 11:18 上午
# @Author  : jing.yang

"""
------老师作业练习------
1、补全计算器（加、除）的测试用例
2、使用参数化完成测试用例自动生成
3、在调用测试方法之前打印【开始计算】，调用方法之后打印【计算结束】
"""
import logging

import allure
import pytest
import yaml
from pythoncode.calculator import Calculator



def get_datas_yaml():
    with open('./test_homework.yml') as f:
        totals = yaml.safe_load(f)
        # return totals
    add_int_datas = totals['add']['int']['datas']
    add_int_ids = totals['add']['int']['datas']
    return  (add_int_datas,add_int_ids)

#测试 获取数据的方法
def test_datas_yaml():
    print(get_datas_yaml())


class TestCalc:
    def setup_class(self):
        print("开始计算")
        logging.info("开始计算")
        #加了self以后，calc就变成实例变量，就可以子啊其他的用例当中调用
        self.calc = Calculator()

    def teardown_class(self):
        print("计算结束")


    @pytest.mark.parametrize('a,b,expect',[
        [0.1,0.1,0.2],
        [0.1, 0.2, 0.3]
    ])
    def test_add_float(self,a,b,expect):
        assert expect ==round((a + b),2)

    @pytest.mark.div
    def test_div(self):
        # 捕获ZeroDivisionError异常
        with pytest.raises(ZeroDivisionError):
            assert 'zero' == self.calc.div(1,0)







