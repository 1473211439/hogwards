#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/18 6:16 下午
# @Author  : jing.yang
"""
1、补全计算器（加、除）的测试用例
2、使用参数化完成测试用例自动生成
3、在调用测试方法之前打印【开始计算】，调用方法之后打印【计算结束】
"""
import allure
import pytest
import yaml
from pythoncode.calculator import Calculator


def get_datas_yaml():
    with open('./datas.yml') as f:
        totals = yaml.safe_load(f)
        return (totals['datas'],totals['ids'])

def test_datas_yaml():
    print(get_datas_yaml())


@pytest.mark.parametrize('a,b,expect', get_datas_yaml()[0], ids=get_datas_yaml()[1])
class TestCalc:
    def setup(self):
        print("开始计算")
        self.calc = Calculator()

    def teardown(self):
        print("计算结束")

    def test_add(self,a,b,expect):
        try:
            assert expect == self.calc.add(a, b)
        except AssertionError as s:
            print(s)

    @pytest.mark.xfail(raises=ZeroDivisionError)
    def test_div(self,a,b,expect):
        try:
            assert expect == self.calc.div(a, b)
        except Exception as e:
            print(e)







