#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/18 4:08 下午
# @Author  : jing.yang

def pytest_collection_modifyitems(items):
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")