#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/1 9:00 下午
# @Author  : jing.yang

from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy

'''
使用appium自动化添加联系人
1，点击【通讯录】
2，点击【添加成员】
3，点击【手动输入添加】
4，输入姓名
5，输入手机号
6，点击【保存】
'''
#添加通讯录
class TestMailList:
    def setup(self):
        #资源准备 打开应用
        desire_cap = {

            'platformName': 'Android',
            'deviceName': 'emulator-5554',
            'platformVersion': '6.0.1',
            'appPackage': 'com.tencent.wework',
            'appActivity': '.launch.WwMainActivity',
            'skipServerInstallation': True,
            'skipDeviceInitialization': True,
            'dontStopAppOnReset': True,  # 不关闭应用
            'autoGrantPermissions': True,  # 自动获取权限
            'noReset': True,  # 不用每次重置 记住用户信息操作 如登陆信息等
            'automationName': "uiautomator2"

        }

        self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desire_cap)
        self.driver.implicitly_wait(10)

    # 资源回收
    def teardown(self):
        self.driver.quit()


    def test_maillist(self):
        '''
        使用appium自动化添加联系人
        1，点击【通讯录】
        2，点击【添加成员】
        3，点击【手动输入添加】
        4，输入姓名
        5，输入手机号
        6，点击【保存】
        7、判断toast
        '''
        # self.driver.find_element_by_xpath("//*[@text='通讯录']").click()
        self.driver.find_element(MobileBy.XPATH,"//*[@text='通讯录']").click()
        self.driver.find_element(MobileBy.ANDROID_UIAUTOMATOR,
                                 'new UiScrollable(new UiSelector().scrollable(true).\
                                 instance(0)).scrollIntoView(new UiSelector().\
                                 text("添加成员").instance(0));').click()

        # self.driver.find_element_by_xpath("//*[@class='android.widget.TextView'and@text='添加成员']").click()
        self.driver.find_element_by_xpath("//*[@text='手动输入添加']").click()
        self.driver.find_element_by_id("com.tencent.wework:id/b09").send_keys("test02")
        self.driver.find_element_by_id("com.tencent.wework:id/f7y").send_keys("18734232923")
        self.driver.find_element_by_xpath("//*[@resource-id='com.tencent.wework:id/ad2']").click()
        print(self.driver.page_source)
        self.driver.find_element_by_xpath("//*[@resource-id='com.tencent.wework:id/hbs']").click()



