#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/12 8:42 下午
# @Author  : jing.yang
import json

import mitmproxy.http
from mitmproxy import http, ctx


class Rewrite:
    def response(self, flow: mitmproxy.http.HTTPFlow):
        """
            The full HTTP response has been read.
        """
        #通过写一个if条件，去配置我们要修改的接口,给定监听定url匹配规则
        if "https://stock.xueqiu.com/v5/stock/batch/quote.json?" in flow.request.pretty_url\
                and "x=" in flow.request.pretty_url:
            # 打印响应体
            ctx.log.info(f"{flow.response.text}")

            # 通过json加载，使用json的loads方法转为字典格式
            # 修改字典中指定位置的数据
            data = json.loads(flow.response.text)
            #调用自己写的递归方法实现翻倍,2
            new_data = self.my_recursion(data,3)
            #递归后把数据返回
            flow.response.text = json.dumps(new_data)

    #写一个递归函数，去翻倍
    def my_recursion(self,data,times=1):
        #先判断是否是一个字典，如果是字典，则通过k拿到v进行递归
        if isinstance(data,dict):
            for k,v in data.items():
                data[k] = self.my_recursion(v)

        #如果是一个列表,也做递归操作，用列表data存起来
        elif isinstance(data,list):
            #把列表的元素遍历，并进行递归计算
            data =[self.my_recursion(i,times) for i in data]
        #递归退出
        elif isinstance(data,float):
            data = data*times
        else:
            data = data
        return  data



addons = [
    Rewrite()
]

if __name__ == '__main__':
    from mitmproxy.tools.main import mitmdump
    #使用debug模式启动mitmdump
    mitmdump(['-s',__file__ ,'-p', '8080', '--set', 'block_global=false'])




