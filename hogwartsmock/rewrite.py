#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/12 8:42 下午
# @Author  : jing.yang
import json

import mitmproxy.http
from mitmproxy import http, ctx


class Rewrite:
    def response(self, flow: mitmproxy.http.HTTPFlow):
        """
            The full HTTP response has been read.
        """
        #通过写一个if条件，去配置我们要修改的接口,给定监听定url匹配规则
        if "https://stock.xueqiu.com/v5/stock/batch/quote.json?" in flow.request.pretty_url\
                and "x=" in flow.request.pretty_url:
            #打印响应体
            ctx.log.info(f"{flow.response.text}")
            #打印类型,打印只接收字符串
            ctx.log.info(str(type(f"{flow.response.text}")))
            #通过json加载，使用json的loads方法转为字典格式
            #修改字典中指定位置的数据

            data = json.loads(flow.response.text)
            n =1
            for i in data["data"]["items"]:
                i ["quote"]["name"] = "slient_0" + str(n)
                n +=1
            # data["data"]["items"][0]["quote"]["name"]= 'slient_01'
            # data["data"]["items"][1]["quote"]["name"] = 'slient_02'
            # data["data"]["items"][2]["quote"]["name"] = 'slient_03'
            # data["data"]["items"][3]["quote"]["current"] = '888'
            #把修改后的数据返回回去
            #替换响应体的正文
            flow.response.text =json.dumps(data)



addons = [
    Rewrite()
]

if __name__ == '__main__':
    from mitmproxy.tools.main import mitmdump
    #使用debug模式启动mitmdump
    mitmdump(['-s',__file__ ,'-p', '8080', '--set', 'block_global=false'])




