#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/13 3:45 下午
# @Author  : jing.yang

import mitmproxy.http
from mitmproxy import http, ctx


class Maplocal11:
    def request(self, flow: mitmproxy.http.HTTPFlow):
        """
            The full HTTP response has been read.
        """
        #通过写一个if条件，去配置我们要修改的接口,给定监听定url匹配规则
        if "https://stock.xueqiu.com/v5/stock/batch/quote.json?" in flow.request.pretty_url\
                and "x=" in flow.request.pretty_url:
            with open("quote.json",encoding='utf-8') as f:
                flow.response = http.HTTPResponse.make(
                    #状态吗响应
                    200,
                    #数据体
                    f.read(),
                )

addons = [
    Maplocal11()
]


if __name__ == '__main__':
    from mitmproxy.tools.main import mitmdump
    #使用debug模式启动mitmdump
    mitmdump(['-s',__file__ ,'-p', '8080', '--set', 'block_global=false'])