#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/12 4:18 下午
# @Author  : jing.yang

"""
Basic skeleton of a mitmproxy addon.

Run as follows: mitmproxy -s anatomy.py
"""
from mitmproxy import ctx


class Counter:
    def __init__(self):
        self.num = 0

    def request(self, flow):
        self.num = self.num + 1
        ctx.log.info("We've seen %d flows" % self.num)


class Counter11:
    def __init__(self):
        self.num = 0

    def request(self, flow):
        self.num = self.num + 1
        ctx.log.info("Counter11 flowa 打印 %d flows" % self.num)


addons = [
    Counter(),Counter11()
]
