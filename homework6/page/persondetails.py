#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/6 4:26 下午
# @Author  : jing.yang
import logging

from appium.webdriver.common.mobileby import MobileBy

from homework6.page.base_page import BasePage
from homework6.page.eidteperson_page import EidtePerson


class PersonalInformationDetails(BasePage):

    #点击编辑成员
    # 点击【编辑】成员
    def eidte_menber(self):
        logging.info('点击【编辑】成员')
        self.find_and_click(MobileBy.XPATH,'//*[@text="编辑成员"]')
    #跳转到编辑成员页面
        return EidtePerson(self.driver)


