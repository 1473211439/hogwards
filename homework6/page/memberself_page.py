#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/6 4:14 下午
# @Author  : jing.yang

'''
个人信息页面
'''
import logging

from appium.webdriver.common.mobileby import MobileBy

from homework6.page.base_page import BasePage
from homework6.page.persondetails import PersonalInformationDetails


class MemberSelf(BasePage):
    logging.info('点击个人信息页面右上角三个点')
    #点击个人信息页面右上角三个点
    def click_self_massage(self):
        self.find_and_click(MobileBy.ID,'com.tencent.wework:id/hc9')

    #跳转到个人信息详情页面
        return PersonalInformationDetails(self.driver)

