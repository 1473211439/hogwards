#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/5 8:08 上午
# @Author  : jing.yang


'''
编辑成员页面输入姓名手机号
'''
from appium.webdriver.common.mobileby import MobileBy
from homework6.page.base_page import BasePage


class EditMemberPage(BasePage):
    def edit_namber(self,name,phonenum):
        #输入姓名、
        # 手机号、
        # 点击保存
        self.find_and_sendkeys(MobileBy.XPATH,"//*[contains(@text,'姓名')]/../android.widget.EditText",name)
        self.find_and_sendkeys(MobileBy.XPATH,"//*[contains(@text,'手机')]/..//android.widget.EditText",phonenum)

        # self.driver.find_element(MobileBy.XPATH,"//*[contains(@text,'姓名')]/../android.widget.EditText").send_keys(name)
        # self.driver.find_element(MobileBy.XPATH,"//*[contains(@text,'手机')]/..//android.widget.EditText").send_keys(phonenum)

        # 查找手机对应的输入框的另一种写法 //*[contains(@text,'手机')]/..//*[@text='必填']
        self.find_and_click(MobileBy.XPATH, "//*[@text='保存']")
        # self.driver.find_element(MobileBy.XPATH, "//*[@text='保存']").click()

        from homework6.page.addmemberlistpage import AddMemberlistPage
        # 跳回手动输入页面页面
        return AddMemberlistPage(self.driver)


