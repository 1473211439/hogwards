#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/5 8:04 上午
# @Author  : jing.yang


"""
通讯录页面
"""
import logging

from homework6.page.addmemberlistpage import AddMemberlistPage
from homework6.page.base_page import BasePage
from homework6.page.memberself_page import MemberSelf



class AddresslistPage(BasePage):

    def goto_addnemberlist(self):
        # click 添加成员
        logging.info('添加成员')
        self.swipe_find('添加成员').click()
        # 通讯录页面跳转到添加成员页面
        return AddMemberlistPage(self.driver)

    def click_member_name(self,name):
        logging.info('滑动找已添加的姓名')
        self.swipe_find(name).click()
        return MemberSelf(self.driver)

    def get_member_names(self):
        member_name_list = []
        member_names =self.driver.find_elements_by_xpath(
            "//*[@text='企业通讯录']/../following-sibling::android.widget.RelativeLayout//android.widget.TextView")
        for i in member_names:
            member_name_list.append(i.text)
            print(member_name_list)
            print(f"点成员姓名：{member_names}")
        return [member_name for member_name in member_names if member_name !='添加成员' ]













