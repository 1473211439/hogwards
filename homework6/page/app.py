#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/5 7:57 上午
# @Author  : jing.yang
import os

from appium import webdriver
from homework6.page.base_page import BasePage
from homework6.page.mainpage import MainPage


class APP(BasePage):
    '''
    启动APP、重启APP、退出APP
    '''
    def start(self):
        if self.driver == None:
            print("driver==None,创建driver")
            # udid = os.getenv('udid')
            # port = os.getenv('port')
            desire_cap = {

                'platformName': 'android',
                'deviceName': 'emulator-5554',
                # 'platformVersion': '6.0.1',
                'appPackage': 'com.tencent.wework',
                'appActivity': '.launch.LaunchSplashActivity',
                # 'skipServerInstallation': True,#跳过uiAutomator2服务器安装并使用设备中的uiAutomator2服务器。当设备上已安装适当版本的uiAutomator2服务器时，可用于提高启动性能。默认为false
                'skipDeviceInitialization': "true",  # 启动时跳过初始化
                'dontStopAppOnReset': True,  # 不关闭应用
                'autoGrantPermissions': True,  # 自动获取权限
                'noReset': "true",  # 不用每次重置 记住用户信息操作 如登陆信息等
                'automationName': "UiAutomator2"
                # 'newCommandTimeout':6000
                # "udid":"udid" #不同都设备指定不同都udid和端口,可以命令行传递也可以参数化
                #命令行： udid='192.168.57.133:5555' port='4723' pytest test_addcontact.py

            }
            self.driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub", desire_cap)
            # self.driver = webdriver.Remote(f"http://127.0.0.1:{port}/wd/hub", desire_cap)
            self.driver.implicitly_wait(20)

        else:
            print('复用driver')
            #不需要传任何参数启动应用，启动提前配置好对driver,
            self.driver.launch_app()
            # 启动其他应用
            # self.driver.start_activity()

        #启动APP，因为启动完在主页，所以调用自己的goto_main()方法后，把自己的实例应用返回去就可以调用自己的方法
        return self

    def restart(self):
        self.driver.close()
        self.driver.launch_app()#启动

    def quit(self):
        self.driver.quit()

    #跳转到到主页
    def goto_main(self):
        #把driver传递给MainPage
        return MainPage(self.driver)