#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/5 8:00 上午
# @Author  : jing.yang

'''
主页面
'''
import logging

from appium.webdriver.common.mobileby import MobileBy
from appium.webdriver.webdriver import WebDriver

from homework6.page.addresslistpage import AddresslistPage
from homework6.page.base_page import BasePage
from homework6.page.memberself_page import MemberSelf


class MainPage(BasePage):
    #元素封装成元组
    _addresslist_element = (MobileBy.XPATH, "//*[@text='通讯录']")


    def goto_addresslist(self):
        #点击【通讯录】
        logging.info('点击【通讯录】')
        #对上面对元素进行解包
        self.find_and_click(*self._addresslist_element)
        # self.find_and_click(MobileBy.XPATH, "//*[@text='通讯录']")

        #主页面跳转通讯录页面
        return AddresslistPage(self.driver)



