#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/5 8:06 上午
# @Author  : jing.yang


'''
手动输入添加页面
'''
from appium.webdriver.common.mobileby import MobileBy
from homework6.page.base_page import BasePage



class AddMemberlistPage(BasePage):

    def click_addmembermenual(self):
        #click 手动输入添加
        self.find_and_click(MobileBy.XPATH,"//*[@text='手动输入添加']")
        from homework6.page.editmemberpage import EditMemberPage
        return EditMemberPage(self.driver)

    #获取toast，toast在这个界面
    def get_result(self):
        toast = self.get_toast_text()
        #把获取到到toast return回去
        return toast
