#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/6 4:29 下午
# @Author  : jing.yang
import logging

from appium.webdriver.common.mobileby import MobileBy

from homework6.page.base_page import BasePage


class EidtePerson(BasePage):
    def delete_member(self):

        # 点击删除成员
        logging.info('点击删除成员')
        self.swipe_find("删除成员").click()

        logging.info('弹框点击[确定]')
        #弹框点击确定
        self.find_and_click(MobileBy.XPATH, '//*[@text="确定"]')

        #返回到通讯录页面
        from homework6.page.addresslistpage import AddresslistPage
        return AddresslistPage(self.driver)

