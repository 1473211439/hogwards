#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/7 11:57 上午
# @Author  : jing.yang



'''
处理中文的unicode显示问题
'''
def pytest_collection_modifyitems(items):
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")