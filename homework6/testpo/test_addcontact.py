#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/7/5 11:07 上午
# @Author  : jing.yang
import logging

import allure
import pytest
from faker import Faker
# import sys
# sys.path.append("../..")
from homework6.page.app import APP



@allure.feature('通讯录测试')
class TestContact:
    def setup_class(self):
        self.fake = Faker('zh_cn')
        # 启动应用，不能放在setup中 要不就会每执行测试用例都会重启，创建新都实例
        self.app = APP()
        self.name = self.fake.name()
        self.phonenum = self.fake.phone_number()

    def setup(self):
        #从主页执行
        self.main = self.app.start().goto_main()


    def teardown(self):
        self.app.back(1)
        # self.app.restart()


    def teardown_class(self):
        self.app.quit()


    @allure.title('添加联系人')
    @pytest.mark.run(order=1)
    def test_addcontact(self):
        #测试用例就从主页到顺序页面到执行
        result =  self.main.goto_addresslist().goto_addnemberlist()\
            .click_addmembermenual().edit_namber(self.name,self.phonenum).get_result()
        assert '添加成功' == result

    @allure.title('删除联系人')
    @pytest.mark.run(order=2)
    def test_delcontact(self):
        '''
        1，主页面点击通讯录
        2，通讯录页面点击已添加都用户姓名
        3，个人信息页面点右上角三个点
        4，个人信息页面详情页面点击【编辑成员】
        5，编辑成员详情页面点击删除
        6，弹框确认
        7，成功后回到通讯录页面
        '''
        self.main.goto_addresslist().click_member_name(self.name)\
            .click_self_massage().eidte_menber().delete_member()















