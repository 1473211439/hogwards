#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/8/2 15:21
# @Author  : jing.yang
'''
测试肇庆计费，按阶梯计费

'''
import json

import pytest
import yaml

import requests
from hamcrest import *

def get_calc_data():
    '''#获取测试数据'''
    with open('./data_zhaoqing.yml') as f:
        data = yaml.safe_load(f)
    return (data['data'])
def test_getdata():
    print(get_calc_data())


#request.param 实现了数据的获取
#ids 为每条数据起个别名，fixture 实现
@pytest.fixture(params=get_calc_data()[0],ids=get_calc_data()[1])
def get_datas(request):
    return request.param


class Test_zhaoqing:

    def test_jifei(self, parkFee):

        url = "http://test.tingjiandan.com/toll/jifei/parkFee/cal"

        header = {
            "Content-Type": "application/json"
        }
        data = {
            "pmParkId": "bbc9a40ebbd9447d907cadf45893f148",
            "color": "blue",
            "inDateTime": self.time_in,
            "outDateTime": self.time_out
        }
        data = json.dumps(data)
        r = requests.post(url=url,headers=header,data=data)
        print(r.json())

        assert r.json()["parkFee"]==parkFee


