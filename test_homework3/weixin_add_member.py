#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/27 7:35 下午
# @Author  : jing.yang

'''
作业
1、打开企业微信，用复用的方法获取cookie
2、点击【通讯录】--》【添加成员】--》【输入成员信息】---跳转会通讯录判断是否有这个人
'''

#复用获取cookie信息
import logging
import allure
import yaml
from selenium import webdriver


@allure.title("获取cookie")
def test_login_fuyong():
    opt = webdriver.ChromeOptions()
    opt.debugger_address = "127.0.0.1:9222"
    driver = webdriver.Chrome(options=opt)
    # 获取cookie
    cookies = driver.get_cookies()
    with open("data_homecookies.yaml", 'w', encoding="UTF-8") as f:
        yaml.dump(cookies, f)


@allure.title("添加成员")
@allure.story("添加成员测试且断言")
def test_wework():
    driver = webdriver.Chrome()
    #窗口最大化否则元素找不到
    driver.maximize_window()
    #先给个地址否则走data域名
    driver.get("https://work.weixin.qq.com/wework_admin/frame#index")
    #读取cookie
    with open('data_homecookies.yaml', encoding="UTF-8") as f:
        yaml_datas = yaml.safe_load(f)
        #循环遍历cookie并传入
        for cookie in yaml_datas:
            if 'expiry' in cookie:
                del cookie['expiry']
            driver.add_cookie(cookie)
            logging.info(driver.add_cookie)

    driver.get("https://work.weixin.qq.com/wework_admin/frame")
    driver.implicitly_wait(10)
    driver.find_element_by_id("menu_contacts").click()
    driver.find_element_by_link_text("添加成员").click()
    driver.find_element_by_id("username").send_keys("test01")
    driver.find_element_by_id("memberAdd_acctid").send_keys("1101")
    driver.find_element_by_id("memberAdd_mail").send_keys("1101@qq.com")
    driver.find_element_by_link_text("保存").click()
    eles = driver.find_elements_by_css_selector(".member_colRight_memberTable_td:nth-child(2)")
    for x in eles:
        if x.get_attribute("title") =="test01":
            return True

