#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/27 3:37 下午
# @Author  : jing.yang

from test_selenium.po.main_page import MainPage


class TestAddMember:
    def setup(self):
        self.main = MainPage()

    def teardown(self):
        pass

    def test_add_member(self):
        # 链式调用
        result = self.main.goto_contact().click_add_member().edit_member().get_member_name()
        print(result)
        assert "张三1" in result
