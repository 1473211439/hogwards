#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2021/6/27 3:00 下午
# @Author  : jing.yang

# 添加成员详情页
from selenium import webdriver


class AddMemberPage:

    # 添加成员信息
    def edit_member(self):
        # 局部导入，解决循环导入问题
        from test_selenium.po.contact_page import ContactPage
        opt = webdriver.ChromeOptions()
        opt.debugger_address = "127.0.0.1:9222"
        self.driver = webdriver.Chrome(options=opt)
        self.driver.implicitly_wait(10)
        self.driver.find_element_by_id("username").send_keys("张三1")
        self.driver.find_element_by_id("memberAdd_acctid").send_keys("1236781")
        self.driver.find_element_by_id("memberAdd_mail").send_keys("12300011@qq.com")
        self.driver.find_element_by_css_selector(".js_btn_save").click()
        return ContactPage()
